<?php

namespace App\Helper;

class TokenGenerator {
    public static function generate(int $length = 64) : string
    {
        return bin2hex(random_bytes($length));
    }
}