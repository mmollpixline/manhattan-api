<?php

namespace App\Controller\App;

use App\Entity\Organization;
use App\Entity\Shop;
use App\Form\Shop\DeleteFormType;
use App\Form\Shop\UpdateFormType;
use App\Form\ShopFormType;
use App\Helper\TokenGenerator;
use App\Repository\ShopRepository;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    private ShopRepository $shopRepository;
    private Organization $organization;

    public function __construct(ShopRepository $shopRepository) {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/app/shop", name="app_shop")
     */
    public function index(): Response
    {
        $this->organization = $this->getUser()->getOrganization();
        $shops = $this->organization->getShops();

        return $this->render('app/shop/index.html.twig', [
            'organization' => $this->organization,
            'shops' => $shops
        ]);
    }

    /**
     * @Route("/app/shop/create", name="app_shop_create")
     */
    public function create(Request $request): Response
    {
        $this->organization = $this->getUser()->getOrganization();
        $shop = new Shop();
        $form = $this->createForm(ShopFormType::class, $shop);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setApiToken(TokenGenerator::generate());
            $shop->setRequestCount(0);
            $shop->setRequestLimit(1000);
            $shop->setOrganization($this->organization);
            $shop->setCreatedAt(Carbon::now());
            $shop->setUpdatedAt($shop->getCreatedAt());

            $this->shopRepository->save($shop);

            return $this->redirectToRoute('app_shop');
        }

        return $this->render('app/shop/register.html.twig', [
            'shopForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/app/shop/{shop}/update", name="app_shop_update")
     */

    public function update(Shop $shop, Request $request): Response
    {
        $form = $this->createForm(UpdateFormType::class, $shop);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->shopRepository->save($shop);

            return $this->redirectToRoute('app_shop');
        }

        return $this->render('app/shop/update.html.twig', [
            'shopForm' => $form->createView(),
            'shop' => $shop
        ]);
    }

    /**
     * @Route("/app/shop/{shop}/delete", name="app_shop_delete")
     */
    public function delete(Shop $shop, Request $request): Response
    {
        $form = $this->createForm(DeleteFormType::class, $shop);
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $this->shopRepository->delete($shop);

            return $this->redirectToRoute('app_shop');
        }

        return $this->render('app/shop/delete.html.twig', [
            'shopForm' => $form->createView(),
            'shop' => $shop
        ]);
    }

    /**
     * @Route("/app/shop/{shop}", name="app_shop_view")
     */
    public function view(Shop $shop)
    {
        return $this->render('app/shop/view.html.twig', [
            'shop' => $shop,
        ]);
    }
}
