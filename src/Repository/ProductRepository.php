<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[] Returns an array of Product objects
     */

    public function findByEan($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.ean = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return ArrayCollection Returns an array of Product objects
     */

    public function findByMultipleEans($values)
    {
        return new ArrayCollection(
            $this->createQueryBuilder('p')
            ->andWhere('p.ean IN (:values)')
            ->setParameter('values', $values)
            ->getQuery()
            ->getResult()
        );
    }
}
