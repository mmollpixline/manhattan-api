<?php

namespace App\Controller\Api;


use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("/api/product", name="api_list_products")
     */

    public function listAction(ProductRepository $productRepository, SerializerInterface $serializer) : Response {

        $products = $productRepository->findByMultipleEans(['test', 'test2']);

        return new JsonResponse(
            $serializer->serialize($products, 'json', ['groups' => 'public_product']),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/api/product/{ean}", name="api_single_product")
     */

    public function showAction(string $ean, ProductRepository $productRepository, SerializerInterface $serializer) : Response {
        $product = $productRepository->findByEan($ean);

        return new JsonResponse(
            $serializer->serialize($product, 'json', ['groups' => 'public_product']),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
