<?php

namespace App\Repository;

use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    /**
     * @param Shop $shop
     * @return Shop
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Shop $shop) : Shop
    {
        $this->_em->persist($shop);
        $this->_em->flush();

        return $shop;
    }

    /**
     * @param Shop $shop
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Shop $shop): void
    {
        $this->_em->remove($shop);
        $this->_em->flush();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function incrementRequestCount(Shop $shop) : void
    {
        $shop->setRequestCount($shop->getRequestCount() + 1);
        $this->_em->persist($shop);
        $this->_em->flush();
    }

    /**
     * Resets the request_count property of all Shops.
     * This should be done automatically once a month as we limit
     * the use of the api to a certain amount of requests per month.
     * The preferred time to do this is at the first of every month at 0:00
     */
    public function resetRequestCounts() : void
    {
        $query = $this->_em->createQuery('UPDATE App\Entity\Shop shop SET shop.requestCount = 0');
        $query->execute();
    }
}
