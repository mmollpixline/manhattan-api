<?php

namespace App\Controller\App;

use App\Form\Organization\UpdateFormType;
use App\Repository\OrganizationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrganizationController extends AbstractController
{
    private OrganizationRepository $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * @Route("/app/organization", name="app_organization")
     */
    public function index(): Response
    {
        $organization = $this->getUser()->getOrganization();

        return $this->render('app/organization/index.html.twig', [
            'id' => $organization->getId(),
            'name' => $organization->getName(),
            'shops' => $organization->getShops(),
        ]);
    }

    /**
     * @Route("/app/organization/update", name="app_organization_update")
     */

    public function update(Request $request): Response
    {
        $organization = $this->getUser()->getOrganization();
        $form = $this->createForm(UpdateFormType::class, $organization);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->organizationRepository->save($organization);

            return $this->redirectToRoute('app_organization');
        }

        return $this->render('app/organization/update.html.twig', [
            'organizationForm' => $form->createView(),
            'organization' => $organization
        ]);
    }
}
