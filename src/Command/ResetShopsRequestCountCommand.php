<?php

namespace App\Command;

use App\Repository\ShopRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ResetShopsRequestCountCommand extends Command
{
    protected static $defaultName = 'ResetShopsRequestCount';
    protected static $defaultDescription = 'Add a short description for your command';
    private ShopRepository $shopRepository;

    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
        parent::__construct();
    }

    protected function configure(): void
    {}

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->shopRepository->resetRequestCounts();

        $io = new SymfonyStyle($input, $output);

        $io->success('Shop Request counts have been reset successfully.');

        return Command::SUCCESS;
    }
}
